<?php

use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\ProfileController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group([
        'middleware' => 'api',
        'prefix' => 'auth',
        'namespace' => 'Auth'
    ], function(){
        Route::post('register', 'RegisterController');
        Route::post('regenerate_otp', 'RegenerateOtpCodeController');
        Route::post('verification', 'VerificationController');
        Route::post('update-password', 'UpdatePasswordController');
        Route::post('login', 'LoginController');
});

// akses profile
Route::group([
        'middleware' => ['api', 'emailVerified', 'auth:api'],
        'prefix' => 'profile'
    ], function(){
        Route::get('show', [ProfileController::class, 'show']);
        Route::post('update', [ProfileController::class, 'update']);
});
