<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class ProfileController extends Controller
{
    /**
     * Show Pofile
     *
     * @return void
     */
    public function show()
    {
        $data['user'] = Auth::user();

        return response()->json([
            'response_code' => '00',
            'response_message' => 'profile berhasil di tampilkan',
            'data' => $data
        ]);
    }

    /**
     * Update Profile
     *
     * @return void
     */
    public function update(Request $request)
    {
        $user = Auth::user();

        if ($request->hasFile('photo_profile')) {
            $photo_profile           = $request->file('photo_profile');
            $photo_profile_extention = $photo_profile->getClientOriginalExtension();
            $photo_profile_name      = Str::slug($user->name, '-') . '-' . $user->id . '.' . $photo_profile_extention;
            $photo_profile_folder    = '/photos/users/photo-profile';
            $photo_profile_location  = $photo_profile . $photo_profile_name;

            try {
                $photo_profile->move(public_path($photo_profile_folder), $photo_profile_name);
                $user->update([
                    'photo_profile' => $photo_profile_location
                ]);
            } catch (\Throwable $th) {
                return response()->json([
                    'response_code' => '01',
                    'response_message' => 'Gagal upload photo profile',
                ], 200);
            }

        }

        $user->update([
            'name' => $request->name
        ]);

        $data['user'] = $user;

        return response()->json([
            'response_code' => '01',
            'response_message' => 'OTP Code tidak ditemukan',
            'data' => $data
        ], 200);
    }
}
