<?php

namespace App\Http\Controllers\Auth;

use App\Events\UserRegisteredEvent;
use App\Http\Controllers\Controller;
use App\Mail\UserRegisterMail;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'email' => 'required|unique:users,email|email',
            'name' => 'required'
        ]);

        $user = User::create($request->all());

        event(new UserRegisteredEvent($user));

        $data['user'] = $user;

        return response()->json([
            'response_code' => '00',
            'response_message' => 'user baru telah di daftarkan, silahkan check email untuk melihat kode otp',
            'data' => $data
        ]);
    }
}
