<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\OtpCode;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'otp' => 'required'
        ]);

        $now = Carbon::now();
        $otp_code = OtpCode::where('otp', $request->otp)->first();

        // check ketersediaan OTP Code
        if (!$otp_code) {
            return response()->json([
                'response_code' => '01',
                'response_message' => 'OTP Code tidak ditemukan',
            ], 200);
        }

        // apabila otp code ditemukan maka sekarang tinggal dichek valid atau tidak waktunya
        if ($now > $otp_code->valid_until) {
            return response()->json([
                'response_code' => '01',
                'response_message' => 'OTP Code tidak berlaku, silahkan generate ulang',
            ], 200);
        }

        // apabila otp code ditemukan dan valid until juga tidak lebih dari waktu sekarang
        // maka update di email verified di user
        $user = User::find($otp_code->user_id);
        $user->email_verified_at = Carbon::now();
        $user->save();

        // delete otp code di table otp
        $otp_code->delete();

        $data['user'] = $user;

        return response()->json([
            'response_code' => '00',
            'response_message' => 'User berhasil di verifikasi',
            'data'  => $data
        ], 200);

    }
}
