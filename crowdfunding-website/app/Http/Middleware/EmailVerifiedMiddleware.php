<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class EmailVerifiedMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userLogin = Auth::user();

        if ($userLogin->email_verified_at !== null && $userLogin->password !== null ) {
            return $next($request);
        }

        return response()->json([
            'message' => 'Email Anda belum terverifikasi'
        ]);

    }
}
