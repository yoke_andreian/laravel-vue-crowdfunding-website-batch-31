<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userLogin = Auth::user();

        if ($userLogin->isAdmin()) {
            return $next($request);
        }

        return response()->json([
            'message' => 'Anda bukan admin'
        ]);
    }
}
