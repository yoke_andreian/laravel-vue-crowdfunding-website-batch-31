<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Models\Traits\Uuid;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends  Authenticatable implements JWTSubject
{
    use Notifiable, Uuid;

    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'username', 'photo_profile', 'password', 'role_id',
    ];

    /**
     * Relasi one to
     *
     * @return void
     */
    public function role()
    {
        $this->belongsTo(Role::class);
    }

    public function otp()
    {
        $this->hasOne(OtpCode::class);
    }

    public function isAdmin()
    {
        if ($this->role_id === $this->get_admin_role()) {
            return true;
        }
        return false;
    }

    /**
     * get user where column name is admin role admin
     *
     * @return void
     */
    public function get_admin_role()
    {
        $role = Role::where('name', 'admin')->first();
        return $role->id;
    }

    /**
     * get user role
     */
    public function get_user_role()
    {
        $role = Role::where('name', 'user')->first();
        return $role->id;
    }

    /**
     * Ketika creating / membuat user maka kita juga membuat/generate otp code
     *
     */
    public static function boot()
    {
        parent::boot();
        static::creating(function($role){
            $role->role_id == $role->get_user_role();
        });

        static::created(function($model){
            $model->generate_otp_code();
        });

    }

    /**
     * generate otp code
     *
     * @return void
     */
    public function generate_otp_code()
    {
        do {
            $random = mt_rand(100000, 999999);
            $check = OtpCode::where('otp', $random)->first();
        } while ($check);

        $now = Carbon::now();

        // generate / create otp code
        $otpCode = OtpCode::updateOrCreate(
            [
                'user_id' => $this->id
            ],
            [
                'otp' => $random,
                'valid_until' => $now->addMinutes(5)
            ],
        );
    }

}
