<?php

namespace App\Models;

use App\Models\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;

class OtpCode extends Model
{
    use Uuid;

    protected $fillable = [
        'otp',
        'user_id',
        'valid_until'
    ];

    public function users()
    {
        return $this->belongsTo(User::class);
    }
}
