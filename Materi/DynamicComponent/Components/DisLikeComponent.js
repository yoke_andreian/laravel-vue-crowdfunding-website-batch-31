export const DisLikeComponent = {
    template: `
        <div class="">
            <strong>Saya tidak suka</strong>
            <ul>
                <li>
                    Berdebat
                </li>
                <li>
                    Bertengkar
                </li>
                <li>
                    Bergoyang
                </li>
            </ul>
        </div>
    `
};