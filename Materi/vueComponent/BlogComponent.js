export const BlogComponent = {
    template: `
        <div>
            <h5>{{ blog.title }} <button class="btn btn-sm btn-danger" @click="$emit('selected', blog.title)">Pilih</button></h5>
            <h6>{{ blog.content }}</h6>
        </div>
    `,
    props: ['blog'],
} 